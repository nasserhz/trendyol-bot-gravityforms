<?php 

/*
 * Plugin Name:       Trendyol Bot GravityForms Connector
 * Description:       Trigger trendyol after gravity form submit
 * Version:           0.0.1
 * Author:            Naser
 * Text Domain:       trendyol-bot-gravityforms
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

use Src\Trendyol\Trendyol;

// Prefix: tbgfc_
define( 'TBGFC_OPTIONS_KEY', 'tbgfc_options' );

register_activation_hook( __FILE__, function () {
    flush_rewrite_rules(true);
});

register_deactivation_hook( __FILE__, function () {
	flush_rewrite_rules(true);
});


add_action( 'gform_after_submission', 'tbgfc_post_content', 10, 2 );
function tbgfc_post_content( $entry, $form ) {
	$optionKey = TBGFC_OPTIONS_KEY;
	$options = get_option($optionKey);

	if ( ! isset($options['tbgfc_field_gf_form']) || ! intval($options['tbgfc_field_gf_form']) ) {
		return;
	}

	if ( intval($form['id']) !=  intval($options['tbgfc_field_gf_form']) ) {
		return;
	}

	if ( ! isset($options['tbgfc_field_gf_form_field']) || ! intval($options['tbgfc_field_gf_form_field']) ) {
		return;
	}

	$trendyol_url = rgar( $entry, $options['tbgfc_field_gf_form_field'] );

	if (filter_var($trendyol_url, FILTER_VALIDATE_URL)) {
		$trendyol_id = (explode("-", parse_url($trendyol_url)['path']));
		$trendyol_id = (int)end($trendyol_id);
	} else {
		$trendyol_id = (int)$_POST['trendyol_id'];
	}

	$trendyol = new Trendyol();

	$product_detail = $trendyol->getProduct($trendyol_id);

	$images = $product_detail->images;
	$title = $product_detail->name;

	print_r($images);
	echo "<br />";
	print_r($title);

	die();
}


add_action( 'admin_menu', 'tbgfc_options_page' );
function tbgfc_options_page() {
	add_submenu_page(
		'edit.php?post_type=product', 
		'Trendyol Bot Form Connector',
		'اتصال ترندیول به فرم', 
		'manage_options',
		'tbgfc_settings',
        'tbgfc_options_page_function',
        90
	);
}

function tbgfc_settings_init() {
	register_setting( 'tbgfc_settings', 'tbgfc_options' );

	add_settings_section(
		'tbgfc_section_general',
		'انتخاب گرویتی فرم', 
		function ($args) { return; },
		'tbgfc'
	);

	add_settings_field(
		'tbgfc_field_gf_form',
		"فرم",
		'tbgfc_field_gf_form_callback',
		'tbgfc',
		'tbgfc_section_general',
		array(
			'label_for'         => 'tbgfc_field_gf_form',
			'class'             => 'tbgfc_row',
			'related_field'		=> 'tbgfc_field_gf_form_field',
		)
	);

	add_settings_field(
		'tbgfc_field_gf_form_field',
		"فیلد مربوط به آدرس محصول",
		'tbgfc_field_gf_form_field_callback',
		'tbgfc',
		'tbgfc_section_general',
		array(
			'label_for'         => 'tbgfc_field_gf_form_field',
			'class'             => 'tbgfc_row',
			''
		)
	);

}
add_action( 'admin_init', 'tbgfc_settings_init' );


function tbgfc_field_gf_form_field_callback( $args ) {

	$optionKey = TBGFC_OPTIONS_KEY;

	$options = get_option($optionKey);

	$gf_forms = GFAPI::get_forms();

	$forms_list = [];
	foreach ($gf_forms as $key => $form) {
		$forms_list[] = [
			'id' =>  $form['id'],
			'title' =>  $form['title'],
			'fields' =>  $form['fields'],
		];
	}

	$selected_form_fields = [];
	if (isset($options['tbgfc_field_gf_form'])) {
		$selected_form = array_filter($forms_list, function($item) use ($options) {
			return intval($item['id']) == intval($options['tbgfc_field_gf_form']);
		});
		$selected_form_fields = array_map(function($field){
			return [
				'id' => $field['id'],
				'label' => $field['label']
			];
		}, reset($selected_form)['fields']);		
	}

	if (count($selected_form_fields)) {
		echo sprintf(
			'<select name="%s[%s]">',
			$optionKey,
			$args['label_for'],
		);		
		foreach ($selected_form_fields as $key => $fields) {
			echo sprintf(
				'<option value="%s" %s>%s</option>',
				$fields['id'],
				isset($options[$args['label_for']]) ? ( selected( $options[ $args['label_for'] ], $fields['id'], false ) ) : ( '' ),
				$fields['label'],
			);
		}
		echo '</select><p class="description">'.$args['description'].'</p>';
	} else {
		echo 'فیلدی برای انتخاب پیدا نشد! ابتدا ابتدا فرم را انتخاب کنید';
	}
}


function tbgfc_field_gf_form_callback( $args ) {

	$optionKey = TBGFC_OPTIONS_KEY;

	$options = get_option( $optionKey );

	$gf_forms = GFAPI::get_forms();

	$forms_list = [];
	foreach ($gf_forms as $key => $form) {
		$forms_list[] = [
			'id' =>  $form['id'],
			'title' =>  $form['title'],
			'fields' =>  $form['fields'],
		];
	}

	if (count($forms_list)) {
		echo sprintf(
			'<select name="%s[%s]">',
			$optionKey,
			$args['label_for'],
		);		
		foreach ($forms_list as $key => $form) {
			echo sprintf(
				'<option value="%s" %s>%s</option>',
				$form['id'],
				isset($options[$args['label_for']]) ? ( selected( $options[ $args['label_for'] ], $form['id'], false ) ) : ( '' ),
				$form['title'],
			);
		}
		echo '</select><p class="description">'.$args['description'].'</p>';
	} else {
		echo 'فرمی برای انتخاب پیدا نشد!';
	}
}

function tbgfc_options_page_function() {
	if ( ! current_user_can( 'manage_options' ) ) {
		return;
	}
	if ( isset( $_GET['settings-updated'] ) ) {
		add_settings_error( 'tbgfc_messages', 'tbgfc_message', 'Settings Saved', 'updated' );
	}
	settings_errors( 'tbgfc_messages' );
	?>
	<div class="wrap">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<form action="options.php" method="post">
			<?php
			settings_fields( 'tbgfc_settings' );
			do_settings_sections( 'tbgfc' );
			submit_button( 'Save Settings' );
			?>
		</form>
	</div>
	<?php
}
